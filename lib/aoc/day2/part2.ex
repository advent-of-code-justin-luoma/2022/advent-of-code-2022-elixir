defmodule Aoc.Day2.Part2 do
  alias Aoc.Day2.Part1

  @rock {:rock, 1}
  @paper {:paper, 2}
  @scissors {:scissors, 3}

  @lose 0
  @draw 3
  @win 6

  def part2() do
    "./inputs/day2"
    |> File.stream!()
    |> Enum.map(&String.trim/1)
    |> Enum.map(&parse/1)
    |> Enum.map(&play/1)
    |> Enum.sum()
  end

  def example() do
    "./inputs/day2example"
    |> File.stream!()
    |> Enum.map(&String.trim/1)
    |> Enum.map(&parse/1)
    |> Enum.map(&play/1)
    |> Enum.sum()
  end

  def play({{:rock, _} = opponent, :win}), do: Part1.score({opponent, @paper})
  def play({{:rock, _} = opponent, :lose}), do: Part1.score({opponent, @scissors})
  def play({{:rock, _} = opponent, :draw}), do: Part1.score({opponent, @rock})

  def play({{:paper, _} = opponent, :win}), do: Part1.score({opponent, @scissors})
  def play({{:paper, _} = opponent, :lose}), do: Part1.score({opponent, @rock})
  def play({{:paper, _} = opponent, :draw}), do: Part1.score({opponent, @paper})

  def play({{:scissors, _} = opponent, :win}), do: Part1.score({opponent, @rock})
  def play({{:scissors, _} = opponent, :lose}), do: Part1.score({opponent, @paper})
  def play({{:scissors, _} = opponent, :draw}), do: Part1.score({opponent, @scissors})

  def parse(line), do: line |> String.split(" ") |> _parse()

  defp _parse([opponent, player]) do
    {
      opponent
      |> convert(),
      player
      |> convert()
    }
  end

  defp convert("A"), do: @rock
  defp convert("X"), do: :lose
  defp convert("B"), do: @paper
  defp convert("Y"), do: :draw
  defp convert("C"), do: @scissors
  defp convert("Z"), do: :win
end
