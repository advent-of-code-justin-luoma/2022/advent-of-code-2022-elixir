defmodule Aoc.Day2.Part1 do
  @rock {:rock, 1}
  @paper {:paper, 2}
  @scissors {:scissors, 3}

  @lose 0
  @draw 3
  @win 6

  def example() do
    "./inputs/day2example"
    |> File.stream!()
    |> Enum.map(&String.trim/1)
    |> Enum.map(&parse/1)
    |> Enum.map(&score/1)
    |> Enum.sum()
  end

  def part1() do
    "./inputs/day2"
    |> File.stream!()
    |> Enum.map(&String.trim/1)
    |> Enum.map(&parse/1)
    |> Enum.map(&score/1)
    |> Enum.sum()
  end

  def score({{:rock, _}, {:paper, points}}), do: points + @win
  def score({{:rock, _}, {:scissors, points}}), do: points + @lose
  def score({{:rock, _}, {:rock, points}}), do: points + @draw

  def score({{:paper, _}, {:scissors, points}}), do: points + @win
  def score({{:paper, _}, {:rock, points}}), do: points + @lose
  def score({{:paper, _}, {:paper, points}}), do: points + @draw

  def score({{:scissors, _}, {:rock, points}}), do: points + @win
  def score({{:scissors, _}, {:paper, points}}), do: points + @lose
  def score({{:scissors, _}, {:scissors, points}}), do: points + @draw

  def parse(line), do: line |> String.split(" ") |> _parse()

  defp _parse([opponent, player]) do
    {
      opponent
      |> convert(),
      player
      |> convert()
    }
  end

  defp convert("A"), do: @rock
  defp convert("X"), do: @rock
  defp convert("B"), do: @paper
  defp convert("Y"), do: @paper
  defp convert("C"), do: @scissors
  defp convert("Z"), do: @scissors
end
