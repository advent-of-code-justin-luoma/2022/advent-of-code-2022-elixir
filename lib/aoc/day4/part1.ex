defmodule Aoc.Day4.Part1 do
  def part1() do
    "./inputs/day4"
    |> solve(filter())
  end

  def example() do
    "./inputs/day4example"
    |> solve(filter())
  end

  def filter() do
    fn {{r1, r2}, {r3, r4}} ->
      (r1 <= r3 && r2 >= r4) || (r3 <= r1 && r4 >= r2)
    end
  end

  def solve(input, filter) do
    input
    |> parse_input()
    |> Enum.map(&String.trim/1)
    |> Enum.map(&String.split(&1, ",", trim: true))
    |> Enum.map(&parse_pair/1)
    |> Enum.filter(filter)
    |> length()
  end

  def parse_input(file) do
    file
    |> File.stream!()
  end

  def parse_pair([pair1, pair2]),
    do: {parse(pair1 |> String.split("-")), parse(pair2 |> String.split("-"))}

  def parse([v1, v2]), do: {String.to_integer(v1), String.to_integer(v2)}
end
