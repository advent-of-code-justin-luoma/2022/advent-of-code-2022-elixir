defmodule Aoc.Day4.Part2 do
  alias Aoc.Day4.Part1

  def part2() do
    "./inputs/day4"
    |> Part1.solve(filter())
  end
  def example() do
    "./inputs/day4example"
    |> Part1.solve(filter())
  end

  def filter() do
    fn {{r1, r2}, {r3, r4}} ->
      (r2 >= r3 && r1 <= r3) || (r1 >= r3 && r4 >= r1)
    end
  end
end
