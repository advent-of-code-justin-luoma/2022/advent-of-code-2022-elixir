defmodule Aoc.Day5.Part1 do
  def part1() do
    "./inputs/day5"
    |> get_instructions()
    |> solve(%{
      1 => ["R", "W", "F", "H", "T", "S"],
      2 => ["W", "Q", "D", "G", "S"],
      3 => ["W", "T", "B"],
      4 => ["J", "Z", "Q", "N", "T", "W", "R", "D"],
      5 => ["Z", "T", "V", "L", "G", "H", "B", "F"],
      6 => ["G", "S", "B", "V", "C", "T", "P", "L"],
      7 => ["P", "G", "W", "T", "R", "B", "Z"],
      8 => ["R", "J", "C", "T", "M", "G", "N"],
      9 => ["W", "B", "G", "L"]
    })
  end

  def example() do
    "./inputs/day5example"
    |> get_instructions()
    |> solve(%{
      1 => ["N", "Z"],
      2 => ["D", "C", "M"],
      3 => ["P"]
    })

    # |> (fn ins ->
    #       move(
    #         %{
    #           1 => ["N", "Z"],
    #           2 => ["D", "C", "M"],
    #           3 => ["P"]
    #         },
    #         ins
    #       )
    #     end).()
  end

  def solve(ins, stacks) do
    move(stacks, ins)
  end

  def get_instructions(file) do
    [_, ins | _] =
      file
      |> File.read!()
      |> String.split("\r\n\r\n", trim: true)

    ins
    |> String.split("\r\n", trim: true)
    |> Enum.map(&String.split/1)
    |> Enum.map(&parse/1)
  end

  def parse([_, n, _, from, _, to]) do
    n = String.to_integer(n)
    from = String.to_integer(from)
    to = String.to_integer(to)

    {n, from, to}
  end

  def move(stacks, []), do: stacks

  def move(stacks, [{n, from, to} | rest]) do
    src = stacks[from]
    dst = stacks[to]

    {src, dst} = move_crate(src, dst, n)

    stacks =
      stacks
      |> Map.put(from, src)
      |> Map.put(to, dst)

    move(stacks, rest)
  end

  def move_crate(from, to, 0), do: {from, to}

  def move_crate(from, to, n) do
    [hd | rest] = from
    to = [hd | to]

    move_crate(rest, to, n - 1)
  end
end
