defmodule Aoc.Day5.Part2 do
  alias Aoc.Day5.Part1

  def part2() do
    "./inputs/day5"
    |> Part1.get_instructions()
    |> solve(%{
      1 => ["R", "W", "F", "H", "T", "S"],
      2 => ["W", "Q", "D", "G", "S"],
      3 => ["W", "T", "B"],
      4 => ["J", "Z", "Q", "N", "T", "W", "R", "D"],
      5 => ["Z", "T", "V", "L", "G", "H", "B", "F"],
      6 => ["G", "S", "B", "V", "C", "T", "P", "L"],
      7 => ["P", "G", "W", "T", "R", "B", "Z"],
      8 => ["R", "J", "C", "T", "M", "G", "N"],
      9 => ["W", "B", "G", "L"]
    })
  end

  def example() do
    "./inputs/day5example"
    |> Part1.get_instructions()
    |> solve(%{
      1 => ["N", "Z"],
      2 => ["D", "C", "M"],
      3 => ["P"]
    })
  end

  def solve(ins, stacks) do
    move(stacks, ins)
  end

  def move(stacks, []), do: stacks

  def move(stacks, [{n, from, to} | rest]) do
    src = stacks[from]
    dst = stacks[to]

    {src, dst} = move_crates(src, dst, n)

    stacks =
      stacks
      |> Map.put(from, src)
      |> Map.put(to, dst)

    move(stacks, rest)
  end

  def move_crates(from, to, n) do
    {move, from} = Enum.split(from, n)
    # from = Enum.take(from, n)
    to = move ++ to

    {from, to}
  end
end
