defmodule Aoc.Day3.Part1 do

  def part1() do
    "./inputs/day3"
    |> solve()
  end

  def example() do
    "./inputs/day3example"
    |> solve()
  end

  def solve(input) do
    input
    |> File.stream!()
    |> Enum.map(&String.trim/1)
    |> Enum.map(&parse/1)
    |> Enum.map(&priority/1)
  end

  def parse(rucksack) do
    len = rucksack
    |> String.length()

    (rucksack
    |> String.split_at(div(len, 2))
    |> (fn {p1, p2} -> String.myers_difference(p1, p2) end).())[:eq]
    |> String.first()
  end

  def priority(<<v::utf8>>) when v > 96 do
    v - 96
  end

  def priority(<<v::utf8>>) do
    v - 64 + 26
  end
end
