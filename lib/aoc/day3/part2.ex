defmodule Aoc.Day3.Part2 do
  alias Aoc.Day3.Part1

  def part2() do
    "./inputs/day3"
    |> solve()
  end

  def example() do
    "./inputs/day3example"
    |> solve()
  end

  def solve(input) do
    input
    |> File.stream!()
    |> Enum.map(&String.trim/1)
    |> Enum.chunk_every(3)
    |> Enum.map(fn l ->
      Enum.map(l, fn rucksack ->
        rucksack
        |> String.split("", trim: true)
        |> Enum.sort()
      end)
    end)
    |> Enum.map(fn [a, b, c] -> find_group(a, b, c, {a, b, c}) end)
    |> Enum.map(&Part1.priority/1)
    |> Enum.sum()
  end

  def find_group([group | _], [group | _], [group | _], _), do: group

  def find_group([group | a_rest], [group | b_rest], [c | _], {_, _, c} = initial)
      when c > group do
    find_group(a_rest, b_rest, c, initial)
  end

  def find_group([group | a_rest], [b | _] = b_all, c, initial) when b > group do
    find_group(a_rest, b_all, c, initial)
  end

  def find_group([group | a_rest], [group | b_rest], [], {_, _, c} = initial) do
    find_group(a_rest, b_rest, c, initial)
  end

  def find_group([group | _] = a, [group | _] = b, [_ | c_rest], initial) do
    find_group(a, b, c_rest, initial)
  end

  def find_group(a, [], [_ | c_rest], {_, b, _} = initial) do
    find_group(a, b, c_rest, initial)
  end

  def find_group([_ | a_rest], b, [], {_, _, c} = initial) do
    find_group(a_rest, b, c, initial)
  end

  def find_group(a, [_ | b_rest], c, initial) do
    find_group(a, b_rest, c, initial)
  end
end
