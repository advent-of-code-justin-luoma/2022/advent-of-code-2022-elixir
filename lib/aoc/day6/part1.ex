defmodule Aoc.Day6.Part1 do

  def part1() do
    "./inputs/day6"
    |> File.read!()
    |> String.split("", trim: true)
    |> find_marker([], 0)
  end

  def find_marker(_, set, n) when length(set) == 4, do: {set, n}

  def find_marker([hd | tail], set, n) do
    if Enum.member?(set, hd) do
      set = remove_duplicate(set, hd)
      find_marker(tail, set ++ [hd], n + 1)
    else
      find_marker(tail, set ++ [hd], n + 1)
    end
  end

  def remove_duplicate(list, remove) do
    if Enum.member?(list, remove) do
      remove_duplicate(tl(list), remove)
    else
      list
    end
  end
end
