defmodule Aoc.Day3.Part2Test do
  use ExUnit.Case, async: true
  alias Aoc.Day3.Part2

  test "find_group 1" do
    a = String.split("vJrwpWtwJgWrhcsFMMfFFhFp", "", trim: true) |> Enum.sort()
    b = String.split("jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL", "", trim: true) |> Enum.sort()
    c = String.split("PmmdzqPrVvPwwTWBwg", "", trim: true) |> Enum.sort()

    assert Part2.find_group(a, b, c, {a, b, c}) == "r"
  end

  test "find_group 2" do
    a = String.split("wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn", "", trim: true) |> Enum.sort()
    b = String.split("ttgJtRGJQctTZtZT", "", trim: true) |> Enum.sort()
    c = String.split("CrZsJsPPZsGzwwsLwLmpwMDw", "", trim: true) |> Enum.sort()

    assert Part2.find_group(a, b, c, {a, b, c}) == "Z"
  end
end
