defmodule Aoc.Day2.Part1Test do
	use ExUnit.Case, async: true
	alias Aoc.Day2.Part1

  test "parse" do
    assert Part1.parse("A Y") == {{:rock, 1}, {:paper, 2}}
    assert Part1.parse("B X") == {{:paper, 2}, {:rock, 1}}
    assert Part1.parse("C Z") == {{:scissors, 3}, {:scissors, 3}}
  end

  test "score" do
    assert Part1.parse("A Y") |> Part1.score() == 8
    assert Part1.parse("B X") |> Part1.score() == 1
    assert Part1.parse("C Z") |> Part1.score() == 6
  end
end
