defmodule Aoc.Day2.Part2Test do
	use ExUnit.Case, async: true
	alias Aoc.Day2.Part2

  test "parse" do
    assert Part2.parse("A Y") == {{:rock, 1}, :draw}
    assert Part2.parse("B X") == {{:paper, 2}, :lose}
    assert Part2.parse("C Z") == {{:scissors, 3}, :win}
  end

  test "play" do
    assert Part2.play({{:rock, 1}, :win}) == 8
    assert Part2.play({{:rock, 1}, :lose}) == 3
    assert Part2.play({{:rock, 1}, :draw}) == 4

    assert Part2.play({{:paper, 2}, :lose}) == 1

    assert Part2.play({{:scissors, 3}, :win}) == 7
  end
end
