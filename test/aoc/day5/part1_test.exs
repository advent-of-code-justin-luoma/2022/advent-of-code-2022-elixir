defmodule Aoc.Day5.Part1Test do
  use ExUnit.Case, async: true
  alias Aoc.Day5.Part1

  test "move 1" do
    ins = [{1, 2, 1}]

    assert Part1.move(
             %{
               1 => ["N", "Z"],
               2 => ["D", "C", "M"],
               3 => ["P"]
             },
             ins
           ) == %{1 => ["D", "N", "Z"], 2 => ["C", "M"], 3 => ["P"]}
  end

  test "move 2" do
    ins = [{3, 1, 3}]

    assert Part1.move(%{1 => ["D", "N", "Z"], 2 => ["C", "M"], 3 => ["P"]}, ins) == %{
             1 => [],
             2 => ["C", "M"],
             3 => ["Z", "N", "D", "P"]
           }
  end
end
