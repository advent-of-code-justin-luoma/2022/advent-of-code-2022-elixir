defmodule Aoc.Day5.Part2Test do
	use ExUnit.Case, async: true
	alias Aoc.Day5.Part2

  test "move_crates" do
    from = ["D", "N", "Z"]
    to = ["P"]

    assert Part2.move_crates(from, to, 3) == {[], ["D", "N", "Z", "P"]}
  end
end
