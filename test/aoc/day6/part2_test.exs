defmodule Aoc.Day6.Part2Test do
	use ExUnit.Case, async: true
	alias Aoc.Day6.Part2

  test "find_marker" do
    input = String.split("mjqjpqmgbljsphdztnvjfqwrcgsmlb", "", trim: true)

    assert Part2.find_marker(input, [], 0) == {["q", "m", "g", "b", "l", "j", "s", "p", "h", "d", "z", "t", "n", "v"], 19}
  end
end
