defmodule Aoc.Day6.Part1Test do
	use ExUnit.Case, async: true
	alias Aoc.Day6.Part1

  test "remove_duplicate" do
    assert Part1.remove_duplicate(["m", "j", "q"], "j") == ["q"]
    assert Part1.remove_duplicate(["b", "v", "w"], "b") == ["v", "w"]
  end

  test "find_marker 1" do
    input = String.split("mjqjpqmgbljsphdztnvjfqwrcgsmlb", "", trim: true)

    assert Part1.find_marker(input, [], 0) == {["j", "p", "q", "m"], 7}
  end

  test "find_marker 2" do
    input = String.split("bvwbjplbgvbhsrlpgdmjqwftvncz", "", trim: true)

    assert Part1.find_marker(input, [], 0) == {["v", "w", "b", "j"], 5}
  end

  test "find_marker 3" do
    input = String.split("nppdvjthqldpwncqszvftbrmjlhg", "", trim: true)

    assert Part1.find_marker(input, [], 0) == {["p", "d", "v", "j"], 6}
  end

  test "find_marker 4" do
    input = String.split("nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg", "", trim: true)

    assert Part1.find_marker(input, [], 0) == {["r", "f", "n", "t"], 10}
  end
end
